package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="History_request")

public class History_request {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String status;
    private long request_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;


}