package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="File_routing")

public class File_routing {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String table_name;
    private String type;
    private long file_id;
    private long table_id;

}