package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Locations")

public class Locations {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String row;
    private String line;
    private String columns;
    private String box;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;


}