package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Case_index")

public class Case_index {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
   private String case_index;
   private String title_ru;
   private String title_kz;
   private String title_en;
   private long storage_type;
   private long storage_year;
   private String note;
   private long company_unit_id;
   private long Nomenclature_id;
   private long created_timestamp;
}
