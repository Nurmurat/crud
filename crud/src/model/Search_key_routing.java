package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Search_key_routing")

public class Search_key_routing {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String table_name;
    private String type;
    private long search_key_id;
    private long table_id;


}