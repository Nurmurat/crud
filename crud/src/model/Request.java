package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Request")

public class Request {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String created_type;
    private String comment;
    private String status;
    private String declinenote;
    private long request_user_id;
    private long response_user_id;
    private long case_id;
    private long case_index_id;
    private long timestamp;
    private long sharestart;
    private long sharefinish;
    private boolean favorite;
    private long updated_timestamp;
    private long updated_by;
    private long company_unit_id;
    private long from_request_id;


}