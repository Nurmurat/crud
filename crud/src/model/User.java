package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="users")

public class User {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String auth_id;
    private String name;
    private String fullname;
    private String surname;
    private String secondname;
    private String status;
    private String password;
    private String lin;
    private boolean is_active;
    private boolean is_activated;
    private long last_login_timestamp;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;


}