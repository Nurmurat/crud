package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Share")

public class Share {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long id;
    private String note;

    private long request_id;
    private long sender_id;
    private long receiver_id;
    private long share_timestamp;

}