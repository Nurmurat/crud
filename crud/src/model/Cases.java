package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Cases")

public class Cases {
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
   private Long id;
    private String case_number;
    private String number_of_tom;
    private String case_heading_ru;
    private String case_heading_kz;
    private String case_heading_en;
    private long start_date;
    private long end_date;
    private long number_page;
    private boolean ESP;
    private String ESP_sign;
    private boolean NAF;
    private boolean deleting;
    private boolean access;
    private String hash;
    private boolean version_activation;
    private String note;
    private long id_location;
    private long case_index_id;
    private long id_record;
    private long company_unit_id;
    private long act_id;
    private String blockchain;
    private long date_add_blockchain;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;
}
